from collections import deque
from threading import Thread
import signal
import time
import pyaudio
from socket import socket, AF_INET, SOCK_DGRAM
import logging

PCM_PAYLOAD_SIZE = 1152            
HEADER_SIZE = 5               
CHUNK_SIZE = (PCM_PAYLOAD_SIZE + HEADER_SIZE)
CHUNK = 288
FORMAT = pyaudio.paInt16
CHANNELS = 1
RATE = 44100
RECORD_SECONDS = 10
SERVER_ADDRESS = ("192.168.2.96",4011)
#SERVER_ADDRESS = ("127.0.0.1",4011)
#SERVER_ADDRESS = ("239.255.77.77",4010)


def recordWorker(pipeline):
    p = pyaudio.PyAudio()

    stream = p.open(format=FORMAT,
                channels=CHANNELS,
                rate=RATE,
                input=True,
                frames_per_buffer=CHUNK)

    logging.info("Started recording")

    while True:
        for i in range(0, int(RATE / CHUNK * RECORD_SECONDS)):
                data = stream.read(CHUNK,exception_on_overflow = False)
                pipeline.append(data)
            
def sendNetWorker(pipeline):
    UDPSock = socket(family=AF_INET,type=SOCK_DGRAM)
    time.sleep(1)
    logging.info("Starting NetSender")
    while True:
            while ( not len(pipeline)==0 ):
                udpPacket = bytearray()
                udpPacket.extend((129).to_bytes(1,"big"))   # currentRate
                udpPacket.extend((16).to_bytes(1,"big"))    # CurrentWidth
                udpPacket.extend((CHANNELS).to_bytes(1,"big"))     # CurrentChannels
                udpPacket.extend(b'\x03')                   # mapping thingy#1
                udpPacket.extend(b'\x00')                   # mapping thingy#2
                
                currentData = pipeline.popleft()
                udpPacket.extend(currentData)
                
                logging.debug("packet length: %s",len(udpPacket))

                UDPSock.sendto(udpPacket, SERVER_ADDRESS)

def handler(signal_received, frame):
    # Handle any cleanup here
    print('SIGINT or CTRL-C detected. Exiting gracefully')
    exit(0)


def main():
    format = "%(asctime)s: %(message)s"
    logging.basicConfig(format=format, level=logging.INFO, datefmt="%H:%M:%S")

    pipeline = deque(maxlen=10)

    recordThread = Thread(target=recordWorker, args=(pipeline,))
    sendNetThread = Thread(target=sendNetWorker, args=(pipeline,))
 
    recordThread.setDaemon(True)
    sendNetThread.setDaemon(True)

    recordThread.start()
    sendNetThread.start()

    signal.signal(signal.SIGINT, handler)

    logging.info('Running. Press CTRL-C to exit.')

    while True:
        time.sleep(1)

if __name__ == "__main__":
    main()
